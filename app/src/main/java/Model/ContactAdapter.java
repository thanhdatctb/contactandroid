package Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.contact.R;

import java.util.Date;
import java.util.List;

public class ContactAdapter extends BaseAdapter {

//    private List<Integer> id;
//    private List<String> name;
//    private List<Boolean> isImportant[];
//    private List<Date> date[];
    private  List<Contact> contacts;
    private Context context;
    public ContactAdapter( Context context, List<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return contacts.get(position).getId();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View item = LayoutInflater.from(context).inflate(R.layout.list_view_item,parent,false);
        TextView Name = (TextView) item.findViewById(R.id.contact_name);
        TextView Date = (TextView) item.findViewById(R.id.date);
        Name.setText(contacts.get(position).getName());
        return item;
    }
}
