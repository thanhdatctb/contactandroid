package Model;

import java.io.Serializable;
import java.util.Date;

public class Contact implements Serializable {
    private int id;
    private String name;
    private boolean isImportant;
    private Date date;

    public Contact() {
    }

    public Contact(String name, boolean isImportant, Date date) {
        this.name = name;
        this.isImportant = isImportant;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
