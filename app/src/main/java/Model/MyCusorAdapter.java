package Model;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.contact.R;

public class MyCusorAdapter extends CursorAdapter {


    public MyCusorAdapter(Context context, Cursor c) {
        super(context, c, false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_view_item,parent,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name = (TextView) view.findViewById(R.id.contact_name);
        TextView date = (TextView) view.findViewById(R.id.date);
        TextView important = (TextView) view.findViewById(R.id.important);
        name.setText(cursor.getString(cursor.getColumnIndex("name")));
        int isimportant = cursor.getInt(cursor.getColumnIndex("isimportant"));
        if(isimportant==1){
            important.setBackgroundColor(Color.GREEN);
        }
        else {
            important.setBackgroundColor(Color.RED);
        }
    }
}
