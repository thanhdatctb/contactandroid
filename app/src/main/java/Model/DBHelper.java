package Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "Contact.db";
    public static final int VERSION = 1;
    public static final String TABLENAME = "Contact";
    public DBHelper(@Nullable Context context) {

        super(context, DBNAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
             db.execSQL("create table " + TABLENAME
                +"(_id integer primary key autoincrement,name text,date text,isimportant integer)");

//        getWritableDatabase().execSQL("insert into "+TABLENAME+"(name, date, isimportant)values('cdf', '12/11/11',1)");
        Log.i("hihi", "kkkk");
        ContentValues values = new ContentValues();
        values.put("name", "abc");
        values.put("date", "def");
        values.put("isimportant", 2);
       db.insert(TABLENAME, null,values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+TABLENAME);
        onCreate(db);
    }
}
