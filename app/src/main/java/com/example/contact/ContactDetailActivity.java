package com.example.contact;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import Model.Contact;

public class ContactDetailActivity extends AppCompatActivity {
    TextView txtContactName;
    TextView txtDate;
    TextView txtImportant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        txtContactName = findViewById(R.id.txtContactName);
        txtDate = findViewById(R.id.txtDate);
        txtImportant = findViewById(R.id.txtImportant);
        Intent intent = getIntent();
        Contact contact = (Contact) intent.getSerializableExtra("contact");
        txtImportant.setText(contact.getName());
        txtDate.setText(contact.getDate().toString());
        txtImportant.setText((contact.isImportant()?"yes":"no"));
    }

    protected void back(View v){

    }
}