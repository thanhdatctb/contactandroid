package com.example.contact;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import Controller.DBController;
import Model.Contact;
import Model.MyCusorAdapter;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {
    ListView contactListView;
    View addDialog;
    DBController dbController;
    MyCusorAdapter myCusorAdapter;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         dbController = new DBController(this);

        addDialog = LayoutInflater.from(this).inflate(R.layout.dialog_add,null,false);

        contactListView = (ListView) this.findViewById(R.id.contactListview);
        //ContactAdapter contactAdapter = new ContactAdapter(this, contacts);
        Cursor mycursor =  dbController.getCursor();
        myCusorAdapter = new MyCusorAdapter(this, mycursor);
        contactListView.setAdapter(myCusorAdapter);

        registerForContextMenu(contactListView);
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<Contact> contactList = dbController.getListContact();
                Contact contact = contactList.get(position);
                Intent intent = new Intent(MainActivity.this, ContactDetailActivity.class);
                intent.putExtra("contact", contact);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu,menu);
        return  true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add: {
                ShowAddDialog();
                break;
            }
            case R.id.exit: {
                //System.exit(1);
                finish();
                break;
            }
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    protected void ShowAddDialog(){
        if (this.addDialog.getParent() != null) {
            ((ViewGroup) addDialog.getParent()).removeView(addDialog);
        }
        EditText contact_name = (EditText) addDialog.findViewById(R.id.contact_name);
        CheckBox importantcb =  addDialog.findViewById(R.id.cbimportant);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.create();
        builder.setTitle("Add contact");
        builder.setView(addDialog);
        builder.setPositiveButton("add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Contact contact = new Contact();
                contact.setName(contact_name.getText().toString());
                contact.setDate(new Date());
                boolean important = importantcb.isChecked();
                contact.setImportant(important);
                dbController.insert(contact);
                myCusorAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create();
        builder.show();
    }
}