package Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Model.Contact;
import Model.DBHelper;

public class DBController {
    private  SQLiteDatabase db;
    private DBHelper dbHelper;
    private Cursor cursor;
    public DBController( Context context) {
        this.dbHelper = new DBHelper(context);
        this.db = dbHelper.getWritableDatabase();
        cursor = getCursor();
        //insert(new Contact("contact 1", true, new Date()));
    }

    public List<Contact> getListContact(){
        List<Contact> contacts = new ArrayList<>();
        Cursor cursor = db.rawQuery("select _id, name from "+ DBHelper.TABLENAME, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Contact contact = new Contact();
            //contact.setId(cursor.getInt(0));
            Cursor cursor1 = cursor;
            contact.setName(cursor.getString(cursor.getColumnIndex("name")));
            //contact.setDate(cursor.getDat);
            contacts.add(contact);
            cursor.moveToNext();
        }
        return contacts;
    }
    public void update(Contact contact)
    {
        ContentValues values = new ContentValues();
        values.put("name", contact.getName());
        db.update(DBHelper.TABLENAME, values, "_id = "+contact.getId(), null);

    }
    public void insert(Contact contact){
        cursor.moveToLast();

       // db.execSQL("insert into "+DBHelper.TABLENAME +"(name, date, isimportant) values" +  "(contact.getName() ,"+"20/10/2002,"+ contact.isImportant())")";
        ContentValues values = new ContentValues();
        values.put("name", contact.getName());
        values.put("date", contact.getDate().toString());
        //values.put("isimportant", contact.isImportant());
        if(contact.isImportant()){
            values.put("isimportant", 1);
        }else {
            values.put("isimportant", 0);
        }
        db.insert(DBHelper.TABLENAME, null, values);

    }
    public  boolean delete(int id){
        //db.execSQL("delete from "+ DBHelper.TABLENAME+"where id ="+id);
         db.delete(DBHelper.TABLENAME, "_id = " + id, null);
        return true;
    }
    public boolean addDefaultList(){
        if(getItemCount()==0){

        }
        return  true;
    }
    public  int getItemCount(){
        return this.getListContact().size();
    }

    public  Cursor getCursor(){
        return db.rawQuery("select * from "+DBHelper.TABLENAME, null);
    }
}
